Ddoc

$(DERS_BOLUMU D 语言编程)

<div style="overflow: auto;">

<img style="border-width:0; float:left; margin:0 2em 1em 1em;" src="$(ROOT_DIR)/ders/d.cn/cover_thumb.png" height="180"/>

$(H6 ISBNs)
$(P
978-0-692-59943-3 hardcover by IngramSpark$(BR)
978-0-692-52957-7 paperback by IngramSpark$(BR)
978-1-515-07460-1 paperback by CreateSpace$(BR)
978-1-519-95441-1 ePUB by Draft2Digital$(BR)
)

$(P
以上各个版本受诸多因素影响而存在差异，如价格、运送时间、运送成本、关税与其他费用，以及当地书店是否有售等等
)

</div>

$(P
其他提供形式还包括：$(LINK2 https://gumroad.com/l/PinD, Gumroad提供的$(I 按需支付) 电子书) 以及各种$(I 免费) 版本的电子书，如 $(LINK_DOWNLOAD http://ddili.org/ders/d.cn/Programming_in_D.pdf, PDF)、$(LINK_DOWNLOAD http://ddili.org/ders/d.cn/Programming_in_D.epub, EPUB)、$(LINK_DOWNLOAD http://ddili.org/ders/d.cn/Programming_in_D.azw3, AZW3) 和 $(LINK_DOWNLOAD http://ddili.org/ders/d.cn/Programming_in_D.mobi, MOBI)。
)

$(P
$(LINK_DOWNLOAD /ders/d.cn/Programming_in_D_code_samples.zip, 点击此处可下载 $(C .zip) 文件形式的示例代码。)
)

$(H5 在线版本)

$(P $(LINK2 /ders/d.cn/ix.html, $(B 索引)) （可以使用关键字搜索）)

$(UL
$(WORK_IN_PROCESS
$(LI $(LINK2 /ders/d.cn/foreword1.html, Walter Bright 序))
)
$(LI $(LINK2 /ders/d.cn/foreword2.html, Andrei Alexandrescu 序))
$(LI $(LINK2 /ders/d.cn/preface.html, 前言))
$(LI $(LINK2 /ders/d.cn/hello_world.html, Hello World程序) $(INDEX_KEYWORDS main))
$(LI $(LINK2 /ders/d.cn/writeln.html, writeln 和 write))
$(LI $(LINK2 /ders/d.cn/compiler.html, 编译))
$(LI $(LINK2 /ders/d.cn/types.html, 基础类型) $(INDEX_KEYWORDS char int double (等)))
$(LI $(LINK2 /ders/d.cn/assignment.html, 赋值与计算顺序) $(INDEX_KEYWORDS =))
$(LI $(LINK2 /ders/d.cn/variables.html, 变量))
$(LI $(LINK2 /ders/d.cn/io.html, 标准输入、输出流) $(INDEX_KEYWORDS stdin stdout))
$(LI $(LINK2 /ders/d.cn/input.html, 从标准输入读取))
$(LI $(LINK2 /ders/d.cn/logical_expressions.html, 逻辑表达式) $(INDEX_KEYWORDS bool true false !== != < <= > >= || &&))
$(LI $(LINK2 /ders/d.cn/if.html, if 语句) $(INDEX_KEYWORDS if else))
$(LI $(LINK2 /ders/d.cn/while.html, while 循环) $(INDEX_KEYWORDS while continue break))
$(LI $(LINK2 /ders/d.cn/arithmetic.html, 整型和算术运算) $(INDEX_KEYWORDS ++ -- + - * / % ^^ += -= *= /= %= ^^=))
$(LI $(LINK2 /ders/d.cn/floating_point.html, 浮点类型) $(INDEX_KEYWORDS .nan .infinity isNaN))
$(LI $(LINK2 /ders/d.cn/arrays.html, 数组) $(INDEX_KEYWORDS [] .length ~ ~=))
$(LI $(LINK2 /ders/d.cn/characters.html, 字符) $(INDEX_KEYWORDS char wchar dchar))
$(LI $(LINK2 /ders/d.cn/slices.html, 分片与其他数组功能) $(INDEX_KEYWORDS .. $ .dup capacity))
$(LI $(LINK2 /ders/d.cn/strings.html, 字符串) $(INDEX_KEYWORDS char[] wchar[] dchar[] string wstring dstring))
$(LI $(LINK2 /ders/d.cn/stream_redirect.html, 重定向标准输入输出流))
$(LI $(LINK2 /ders/d.cn/files.html, 文件) $(INDEX_KEYWORDS File))
$(LI $(LINK2 /ders/d.cn/auto_and_typeof.html, auto 和 typeof) $(INDEX_KEYWORDS auto typeof))
$(LI $(LINK2 /ders/d.cn/name_space.html, 名字作用域))
$(LI $(LINK2 /ders/d.cn/for.html, for 循环) $(INDEX_KEYWORDS for))
$(LI $(LINK2 /ders/d.cn/ternary.html, 三元运算符 ?:) $(INDEX_KEYWORDS ?:))
$(LI $(LINK2 /ders/d.cn/literals.html, 文字量))
$(LI $(LINK2 /ders/d.cn/formatted_output.html, 格式化输出) $(INDEX_KEYWORDS writef writefln))
$(LI $(LINK2 /ders/d.cn/formatted_input.html, 格式化输入))
$(LI $(LINK2 /ders/d.cn/do_while.html, do-while 循环) $(INDEX_KEYWORDS do while))
$(LI $(LINK2 /ders/d.cn/aa.html, 关联数组) $(INDEX_KEYWORDS .keys .values .byKey .byValue .byKeyValue .get .remove in))
$(LI $(LINK2 /ders/d.cn/foreach.html, foreach 循环) $(INDEX_KEYWORDS foreach .byKey .byValue .byKeyValue))
$(LI $(LINK2 /ders/d.cn/switch_case.html, switch 和 case) $(INDEX_KEYWORDS switch, case, default, final switch))
$(LI $(LINK2 /ders/d.cn/enum.html, enum) $(INDEX_KEYWORDS enum .min .max))
$(LI $(LINK2 /ders/d.cn/functions.html, 函数) $(INDEX_KEYWORDS return void))
$(LI $(LINK2 /ders/d.cn/const_and_immutable.html, 不变量) $(INDEX_KEYWORDS enum const immutable .dup .idup))
$(LI $(LINK2 /ders/d.cn/value_vs_reference.html, 值类型与引用类型) $(INDEX_KEYWORDS &))
$(LI $(LINK2 /ders/d.cn/function_parameters.html, 函数参数) $(INDEX_KEYWORDS in out ref inout lazy scope shared))
$(LI $(LINK2 /ders/d.cn/lvalue_rvalue.html, 左值与右值) $(INDEX_KEYWORDS auto ref))
$(LI $(LINK2 /ders/d.cn/lazy_operators.html, 惰性运算符))
$(LI $(LINK2 /ders/d.cn/main.html, 程序环境) $(INDEX_KEYWORDS main stderr))
$(LI $(LINK2 /ders/d.cn/exceptions.html, 异常) $(INDEX_KEYWORDS throw try catch finally))
$(LI $(LINK2 /ders/d.cn/scope.html, scope) $(INDEX_KEYWORDS scope(exit) scope(success) scope(failure)))
$(LI $(LINK2 /ders/d.cn/assert.html, assert 与 enforce) $(INDEX_KEYWORDS assert enforce))
$(LI $(LINK2 /ders/d.cn/unit_testing.html, 单元测试) $(INDEX_KEYWORDS unittest))
$(LI $(LINK2 /ders/d.cn/contracts.html, 契约编程) $(INDEX_KEYWORDS in out))
$(LI $(LINK2 /ders/d.cn/lifetimes.html, 生命周期与函数式运算))
$(LI $(LINK2 /ders/d.cn/null_is.html, null 值与 is 运算符) $(INDEX_KEYWORDS null is !is))
$(LI $(LINK2 /ders/d.cn/cast.html, 类型转换) $(INDEX_KEYWORDS to assumeUnique cast))
$(LI $(LINK2 /ders/d.cn/struct.html, 结构) $(INDEX_KEYWORDS struct .
{} static, static this, static ~this))
$(LI $(LINK2 /ders/d.cn/parameter_flexibility.html, 不定个数参数) $(INDEX_KEYWORDS T[]... __MODULE__ __FILE__ __LINE__ __FUNCTION__（等）))
$(LI $(LINK2 /ders/d.cn/function_overloading.html, 函数重载))
$(LI $(LINK2 /ders/d.cn/member_functions.html, 成员函数) $(INDEX_KEYWORDS toString))
$(LI $(LINK2 /ders/d.cn/const_member_functions.html, const ref 参数和 const 函数函数) $(INDEX_KEYWORDS const ref, in ref, inout))
$(LI $(LINK2 /ders/d.cn/special_functions.html, 构造函数和其他特殊函数) $(INDEX_KEYWORDS this ~this this(this) opAssign @disable))
$(LI $(LINK2 /ders/d.cn/operator_overloading.html, 运算符重载) $(INDEX_KEYWORDS opUnary opBinary opEquals opCmp opIndex（等）))
$(LI $(LINK2 /ders/d.cn/class.html, 类) $(INDEX_KEYWORDS class new))
$(LI $(LINK2 /ders/d.cn/inheritance.html, 继承) $(INDEX_KEYWORDS : super override abstract))
$(LI $(LINK2 /ders/d.cn/object.html, Object) $(INDEX_KEYWORDS toString opEquals opCmp toHash typeid TypeInfo))
$(LI $(LINK2 /ders/d.cn/interface.html, 接口) $(INDEX_KEYWORDS interface static final))
$(LI $(LINK2 /ders/d.cn/destroy.html, destroy 和 scoped) $(INDEX_KEYWORDS destroy scoped))
$(LI $(LINK2 /ders/d.cn/modules.html, 模块和库) $(INDEX_KEYWORDS import, module, static this, static ~this))
$(LI $(LINK2 /ders/d.cn/encapsulation.html, 封装和保护属性) $(INDEX_KEYWORDS private protected public package))
$(LI $(LINK2 /ders/d.cn/ufcs.html, 统一调用语法（UFCS）))
$(LI $(LINK2 /ders/d.cn/property.html, 特性) $(INDEX_KEYWORDS @property))
$(LI $(LINK2 /ders/d.cn/invariant.html, 结构和类的契约编程) $(INDEX_KEYWORDS invariant))
$(LI $(LINK2 /ders/d.cn/templates.html, 模板))
$(LI $(LINK2 /ders/d.cn/pragma.html, 编译指令))
$(LI $(LINK2 /ders/d.cn/alias.html, alias 和 with) $(INDEX_KEYWORDS alias with))
$(LI $(LINK2 /ders/d.cn/alias_this.html, alias this) $(INDEX_KEYWORDS alias this))
$(LI $(LINK2 /ders/d.cn/pointers.html, 指针) $(INDEX_KEYWORDS * &))
$(LI $(LINK2 /ders/d.cn/bit_operations.html, 位运算) $(INDEX_KEYWORDS ~ & | ^ >> >>> <<))
$(LI $(LINK2 /ders/d.cn/cond_comp.html, 条件编译) $(INDEX_KEYWORDS debug, version, static if, static assert, __traits))
$(LI $(LINK2 /ders/d.cn/is_expr.html, is 表达式) $(INDEX_KEYWORDS is()))
$(LI $(LINK2 /ders/d.cn/lambda.html, 函数指针、委托和λ) $(INDEX_KEYWORDS function delegate => toString))
$(LI $(LINK2 /ders/d.cn/foreach_opapply.html, 将foreach用于结构和类) $(INDEX_KEYWORDS opApply empty popFront front（等）))
$(LI $(LINK2 /ders/d.cn/nested.html, 嵌套函数、结构和类) $(INDEX_KEYWORDS static))
$(LI $(LINK2 /ders/d.cn/union.html, 联合) $(INDEX_KEYWORDS union))
$(LI $(LINK2 /ders/d.cn/goto.html, 标签和 goto) $(INDEX_KEYWORDS goto))
$(LI $(LINK2 /ders/d.cn/tuples.html, 元组) $(INDEX_KEYWORDS tuple Tuple AliasSeq .tupleof foreach))
$(LI $(LINK2 /ders/d.cn/templates_more.html, 模板的更多内容) $(INDEX_KEYWORDS template opDollar opIndex opSlice))
$(LI $(LINK2 /ders/d.cn/functions_more.html, 函数的更多内容) $(INDEX_KEYWORDS inout pure nothrow @nogc @safe @trusted @system CTFE __ctfe))
$(LI $(LINK2 /ders/d.cn/mixin.html, 混入) $(INDEX_KEYWORDS mixin))
$(LI $(LINK2 /ders/d.cn/ranges.html, 范围) $(INDEX_KEYWORDS InputRange ForwardRange BidirectionalRange RandomAccessRange OutputRange))
$(LI $(LINK2 /ders/d.cn/ranges_more.html, 范围的更多内容) $(INDEX_KEYWORDS isInputRange ElementType hasLength inputRangeObject（等）))
$(LI $(LINK2 /ders/d.cn/parallelism.html, 并行性) $(INDEX_KEYWORDS parallel task asyncBuf map amap reduce))
$(LI $(LINK2 /ders/d.cn/concurrency.html, 消息传递并发) $(INDEX_KEYWORDS spawn thisTid ownerTid send receive (and more)))
$(LI $(LINK2 /ders/d.cn/concurrency_shared.html, 数据共享并发) $(INDEX_KEYWORDS synchronized, shared, shared static this, shared static ~this))
$(LI $(LINK2 /ders/d.cn/fibers.html, 纤程) $(INDEX_KEYWORDS call yield))
$(LI $(LINK2 /ders/d.cn/memory.html, 内存管理) $(INDEX_KEYWORDS calloc realloc emplace destroy .alignof))
$(LI $(LINK2 /ders/d.cn/uda.html, 自定义属性（UDA）) $(INDEX_KEYWORDS @))
$(LI $(LINK2 /ders/d.cn/operator_precedence.html, 运算符优先级))
)

Macros:
        SUBTITLE=D 语言编程

        DESCRIPTION=全新编写的 D 编程语言教程。

        KEYWORDS=d programming language tutorial book novice beginner D 编程语言 教程 书籍 新手 初学者

        BREADCRUMBS=$(BREADCRUMBS_INDEX)

SOZLER=

MINI_SOZLUK=
